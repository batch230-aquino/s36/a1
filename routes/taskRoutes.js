
const { Router } = require("express");
const express = require("express");

// express.Router() method allows access to http methods
const router = express.Router();

const taskController = require("../controllers/taskController");

//Create - task routes
router.post("/addTask", taskController.createTaskController);

//get all task
router.get("/allTasks", taskController.getAllTasksController);

//delete a task
router.delete("/deleteTask/:taskId", taskController.deleteTaskController);


//activity
router.get("/specificTask/:taskId", taskController.getSpecificTaskController);

router.put("/updateStatusTask/:taskId/complete", taskController.updateTaskController);


// Change the status of a task to "complete"
// This route expects to receive a put request at the URL "/tasks/:id/complete"
// The whole URL is at "http://localhost:3001/tasks/:id/complete"
// We cannot use put("/tasks/:id") again because it has already been used in our route to update a task

router.put("/:id/archive", (req, res) => {
	taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 


router.patch("/updateTask/:taskId", taskController.updateTaskNameController)

module.exports = router;

